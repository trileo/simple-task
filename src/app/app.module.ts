import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ServerComponent } from './server/server.component';
import { SamplesComponent } from './samples/samples.component';
import { DevelopersComponent } from './developers/developers.component';
import { DesignerComponent } from './designer/designer.component';
import { TestersComponent } from './testers/testers.component';
import { NewbranchComponent } from './newbranch/newbranch.component';

@NgModule({
  declarations: [
    AppComponent,
    ServerComponent
    SamplesComponent,
    DevelopersComponent,
    DesignerComponent,
    TestersComponent,
    NewbranchComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
