import { SimpleTaskPage } from './app.po';

describe('simple-task App', () => {
  let page: SimpleTaskPage;

  beforeEach(() => {
    page = new SimpleTaskPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
